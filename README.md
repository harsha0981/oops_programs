# Data Validator

* Data validator can be used to validate files such as Csv(Custom Separated Values), Tsv(Tab Separated Values), Txt(A Text file with custom separator) and Json files.

### Goals

* Accept a Multipart file or Path of the file in the form of String and perform validations.

### Available validations

* For Csv,Tsv,Txt files we have below validations

    * Validate extension
    * Validate separator(Validates if the file is actually separated by separator specified by user or not)
    * Validate special character(Validates if any single data point in file is special character or not)
    * Validate length of row(Validates if number of data points in header is equal to number of data points in all rows)
    * Validate separators(Validates if number of separators in header is equal to number of separators  in every row)

* For Json file, we have below validations

    * Validate extension
    * Validate file(Validates if there is syntax mismatch in json file)

### Key features

* The validations that can be performed are not file specific i.e., any format of accepted files can be accepted.
* Eg:
    * Consider a Csv file. The user can validate any Csv file. We are not restricting them in terms of number of rows, number of columns etc.

### Project structure

* All the source code can be found at src/main/java
* We have a package com.xformics.data_validator.validators at root level of source code which contains sub-packages, which contain validators and utilities to support validators
* The remaining information about all the sub-packages, classes present in the packages and methods can be found at the Javadoc.
* [Javadoc](https://xformics.bitbucket.io/)
* Each class has a different method for each validation. So, it is up-to user to choose validation as per his/her requirement.


### Jfrog Repository ###

* The data_validator module is being published as an artifact in to Jfrog artifactory.
* Version : 1.0.0
* [Repository](https://imharshavardhanreddy.jfrog.io/artifactory/validation-gradle-dev-local/)

## Consuming artifact
### How do I get set up for gradle project? ###

* In the build.gradle file of remote project,add following configuration in repositories configuration

```
  maven {
        url "https://imharshavardhanreddy.jfrog.io/artifactory/validation-gradle-dev-local/"
        credentials {
            username = "*******"
            password = "*******"
        }
    }
```
* **To get username and password in order to access Jforg artifact, reach out to people mentioned at the end of the file.**

* Above configuration tells gradle in remote project to browse to url specified to find repository and access it with credentials specified.
* Add below dependency along wth other dependencies of build.gradle file

```
    implementation group: 'com.xformics.data_validator', name: 'data_validator', version: '1.0.0-20230405.063818-1'
```

* The group and name are the group specified in build.gradle and project name specified in settings.gradle files respectively.
* Complete version number should be opted from published artifact of Jfrog.

### How do I get set up for maven project? ###

* In the pom.xml file of remote project,add following configuration in repositories configuration

```
  <repository>
    <id>validation-gradle-dev-local</id>
    <url>https://imharshavardhanreddy.jfrog.io/artifactory/validation-gradle-dev-local/</url>
    <snapshots>
      <enabled>true</enabled>
    </snapshots>
    <releases>
      <enabled>true</enabled>
    </releases>
  </repository> 
```

* Above configuration tells maven in remote project to browse to url specified to find repository, and it can be accessed by specifying credentials in settings.xml file as shown below

```
 
  <settings>
    <servers>
      <server>
        <id>validation-gradle-dev-local</id>
        <username>*******</username>
        <password>*******</password>
      </server>
    </servers>
  </settings>
```
* **To get username and password in order to access Jforg artifact, reach out to people mentioned at the end of the file.**

* Add below dependency along wth other dependencies of build.gradle file

```

  <dependency>
    <groupId>com.xformics.data_validator</groupId>
    <artifactId>data_validator</artifactId>
    <version>1.0.0-20230405.063818-1</version>
  </dependency>
```

* The group and name are the group specified in build.gradle and project name specified in settings.gradle files respectively.
* Complete version number should be opted from published artifact of Jfrog.

### What to do after consuming artifact?

* Once you consume the artifact into your local without any errors, you can see it under **External Libraries** of your project.
* Create an object out of the validator class you want to use and use the methods to perform validations.
* The return value of any method is boolean only.
* You can know in detail about all the classes and methods using the Javadoc.
* [Javadoc](https://xformics.bitbucket.io/)

### Contribution guidelines

* Code review
    * Code is reviewed by sonarLint and a set of guidelines defined and followed to write code.
* [Guidelines folowed](https://xformicsindia.sharepoint.com/:x:/r/sites/Xformics/Shared%20Documents/Career%20tool/XFormics_CodeChecklist.xlsx?d=w40ecaac6e8654aaaa93d63ede01494dd&csf=1&web=1&e=Lvnshc)

### Who do I talk to?

* adarsh.anand@in.xformics.com
* harshavardhan.kogara@in.xformics.com